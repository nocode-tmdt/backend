﻿﻿@extends('user.layouts.master')
@section('title', 'MW Store | Tin tuc' )

@section('content_page')

<div class="breadcrumb-area mt-30">
    <div class="container">
        <div class="breadcrumb">
            <ul class="d-flex align-items-center">
                <li><a href="{{route('home.index')}}">@lang('lang.home')</a></li>
                <li class="active"><a href="{{route('news.index')}}">@lang('lang.news')</a></li>
            </ul>
        </div>
    </div>
</div>


<div class="error404-area ptb-60 ptb-sm-60">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <ul class="main-thumb-desc nav tabs-area" role="tablist">
                    <?php $is_active = true; ?>
                    @foreach ($datas as $data)
                    <li><a class="<?php if ($is_active) {
                                        echo 'active';
                                    }
                                    $is_active = false; ?>" data-toggle="tab" href="#{{$data['key']}}">{{$data['name']}}</a></li>
                    @endforeach
                </ul>

                <div class="tab-content thumb-content">
                    <?php $is_active = true; ?>
                    @foreach ($datas as $data)
                    <div id="{{$data['key']}}" class="tab-pane fade <?php if ($is_active) {
                                                                        echo 'show active';
                                                                    }
                                                                    $is_active = false; ?>">

                        @foreach($data['posts'] as $post)
                        <div class="row row-news">
                            <a target="_blank" href=" {{ $post->get_link() }}">
                                <img class="lazy" data-src=" {{ $post->img }}" alt=" {{ $post->get_title() }}" />
                            </a>
                            <div class="box-post-content">
                                <a target="_blank" class="post-title" href="{{ $post->get_link() }}">
                                    {{ $post->get_title() }}</a>
                                <p class="post-content">{{ $post->desc }}</a></p>
                                <div class="post-info">
                                    <p class="post-info-item post-time">@lang('lang.time'): {{ $post->get_date('j F Y | g:i a') }}</a></p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
