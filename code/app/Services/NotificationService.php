<?php

namespace App\Services;

use App\Repositories\NotificationRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Exception;

class NotificationService
{
    protected $feeshipRepository;

    public function __construct(NotificationRepository $notificationRepository)
    {
        $this->notificationRepository = $notificationRepository;
    }

    public function getTokenByUser($data)
    {
        return $this->notificationRepository->getTokenByUser($data);
    }

    public function getAll()
    {
        return $this->notificationRepository->getAll();
    }

    public function saveUser($data)
    {
        DB::beginTransaction();

        try {
            $this->notificationRepository->saveUser($data);
        } catch (Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());

            throw new Exception($e->getMessage());
        }

        DB::commit();
    }

    public function saveGuest($data)
    {
        DB::beginTransaction();

        try {
            $this->notificationRepository->saveGuest($data);
        } catch (Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());

            throw new Exception($e->getMessage());
        }

        DB::commit();
    }
    public function sendNotification($FcmToken, $data = null)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $serverKey = env('FIREBASE_SERVER_KEY');

        $data = [
            "registration_ids" => $FcmToken,
            "notification" => [
                "title" => $data ? $data['title'] : "Tiêu đề thông báo",
                "body" =>   $data ? $data['body'] : "Nội dung thông báo",
                "image" => ($data && isset($data['image'])) ? $data['image'] : "https://picsum.photos/536/354",
                "icon" => ($data && isset($data['image'])) ? $data['image'] : "https://www.seekpng.com/png/detail/527-5275313_app-icon-detective-conan-avatar.png",
            ],
            "data" => [
                "url" => "https://www.youtube.com/watch?v=8dvocasg1BY"
            ]
        ];
        $encodedData = json_encode($data);

        $headers = [
            'Authorization:key=' . $serverKey,
            'Content-Type: application/json',
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $encodedData);

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }

        // Close connection
        curl_close($ch);
        return  $serverKey;
    }
}
