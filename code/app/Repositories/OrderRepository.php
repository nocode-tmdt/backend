<?php

namespace App\Repositories;

use App\Models\Order;

class OrderRepository
{
    protected $order;
    protected $user_id;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function getCount()
    {
        return $this->order->count();
    }

    public function getAll()
    {
        $user_id = auth()->user()->id;
        return $this->order->with(['user', 'shipping', 'orderDetails', 'coupon'])
            ->where(['user_id' => $user_id])
            ->latest('id')->get();
    }

    public function getAllByAdmin()
    {
        return $this->order->with("user")->latest('id')->get();
    }

    public function getById($id)
    {
        return $this->order->with(['user', 'shipping', 'orderDetails', 'coupon'])->where(['id' => $id])->first();
    }

    public function getByStatus($status)
    {
        $user_id = auth()->user()->id;
        return $this->order->with(['user', 'shipping', 'orderDetails', 'coupon'])->where(['user_id' => $user_id, 'status' => $status])->get();
    }

    public function getByCode($code)
    {
        return $this->order->with(['user', 'shipping', 'orderDetails', 'coupon'])->where(['code' => $code])->first();
    }

    public function getByPrint($print)
    {
        return $this->order->where(['code' => $print[1], "id" => $print[0], "user_id" => $print[2]])->first();
    }

    public function updateStatusByCode($code, $status)
    {
        return $this->order->where("code", $code)->update(['status' => $status]);
    }

    public function save($data)
    {
        $order = new $this->order;
        $order->user_id = auth()->user()->id;
        $order->code = $data['code'];
        $order->coupon_code = $data['coupon_code'];
        $order->status =  $data['status'];
        $order->coupon_money =  $data['coupon_money'];
        $order->feeship_money =  $data['feeship_money'];
        $order->product_money =  $data['product_money'];
        $order->total_money =  $data['total_money'];
        $order->save();
    }

    public function delete($code)
    {
        return $this->order->where('code', $code)->delete();
    }
}
