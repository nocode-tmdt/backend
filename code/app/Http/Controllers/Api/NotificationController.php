<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\NotificationService;
use Exception;

class NotificationController extends Controller
{
    protected $notificationService;

    public function __construct(NotificationService $notificationService)
    {
        $this->notificationService = $notificationService;
    }

    public function send(Request $req)
    {
        if ($req->type == "ALL_USER") {
            $notis = $this->notificationService->getAll();
        } else {
            $notis = $this->notificationService->getTokenByUser($req);
        }

        $tokens = [];
        foreach ($notis as $noti) {
            $tokens[] = $noti->token;
        }

        $image = null;
        if ($req->image != "") {
            $image = $req->image;
        }

        try {
            $this->notificationService->sendNotification($tokens, ["title" => $req->title, "body" => $req->body, "image" => $image]);
            return $this->successResponse($tokens);
        } catch (Exception $e) {

            return $this->errorResponse($e->getMessage());
        }
    }

    public function get_token(Request $req)
    {
        $tokens = $this->notificationService->getTokenByUser($req);
        return $this->successResponse($tokens);
    }

    public function all_token()
    {
        $tokens = $this->notificationService->getAll();
        return $this->successResponse($tokens);
    }

    public function save_token(Request $req)
    {
        $result = $this->notificationService->sendNotification([$req->token]);
        try {
            if (auth()->user()) {
                $this->notificationService->saveUser($req);
                return $this->successResponse($result);
            } else {
                $this->notificationService->saveGuest($req);
            }
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage());
        }
    }
}
