<?php

namespace App\Http\Controllers;

use App\Models\News;

class NewsController extends Controller
{
    public function index()
    {
        $posts = cache()->remember('news', 600, function () {
            return  News::getNews();
        });
        return view('user.utils.news')->with(compact(['posts']));
    }

    public function rssNew()
    {

        $RSSlist = [
            [
                'name' => "Tin mới nhất",
                'key' => "news",
                "url" => "https://vnexpress.net/rss/tin-moi-nhat.rss",
            ],
            [
                'name' => "Khoa học",
                'key' => "science",
                "url" => "https://vnexpress.net/rss/khoa-hoc.rss",
            ],
            [
                'name' => "Sức khỏe",
                'key' => "health",
                "url" => "https://vnexpress.net/rss/suc-khoe.rss",
            ],
            [
                'name' => "Đời sống",
                'key' => "life",
                "url" => "https://vnexpress.net/rss/gia-dinh.rss",
            ],
            [
                'name' => "Pháp luật",
                'key' => "law",
                "url" => "https://vnexpress.net/rss/phap-luat.rss",
            ],

        ];

        $datas = [];

        foreach ($RSSlist as $rss) {
            $feed = \Feeds::make($rss["url"]);
            $items = $feed->get_items();
            $posts = [];

            foreach ($items as $post) {
                $data = $post->get_item_tags('', 'description')[0]['data'];
                $post->desc = strip_tags($data);

                $pattern = '/src="([^"]*)"/';
                preg_match($pattern, $data, $result);
                $post->img = isset($result[1]) ? $result[1] : null;

                $posts[] = $post;
            }

            $data = [
                'name' => $rss["name"],
                'key' => $rss["key"],
                'posts' => $posts,
            ];

            $datas[] = $data;
        }

        return view('user.utils.rss')->with(compact(['datas']));
    }
}
