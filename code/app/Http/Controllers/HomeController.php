<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Brand;
use App\Models\Slider;

class HomeController extends Controller
{
    public function index()
    {
        $sliders =
            Slider::with(['product'])->latest('id')->take(10)->get();
        $product_feathers =
            Product::where('feather', 1)->latest('id')->take(15)->get();
        $product_news =
            Product::latest('id')->take(15)->get();
        $brands =
            Brand::with(['products'])->latest('id')->get();

        return view('user.index')->with(compact(['brands', 'sliders', 'product_feathers', 'product_news']));
    }
}
